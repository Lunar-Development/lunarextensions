using LunarUtilExtensions;

namespace LunarTesting;

public class Tests
{
    private List<int> _list;

    [SetUp]
    public void Setup()
    {
        _list = new List<int> {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    }

    [Test]
    public void TestSplit()
    {
        List<List<int>> splitList = _list.Split(3);
        Assert.Multiple(() =>
        {
            Assert.That(splitList, Has.Count.EqualTo(4));
            Assert.That(splitList[0], Has.Count.EqualTo(3));
            Assert.That(splitList[1], Has.Count.EqualTo(3));
            Assert.That(splitList[2], Has.Count.EqualTo(3));
            Assert.That(splitList[3], Has.Count.EqualTo(1));
        });
    }

    [Test]
    public void TestIsEmptyOrNull()
    {
        List<int> emptyList = new();
        Assert.Multiple(() =>
        {
            Assert.That(_list.IsEmptyOrNull(), Is.False);
            Assert.That(emptyList.IsEmptyOrNull(), Is.True);
        });
    }

    [Test]
    public void TestMaxOrDefault()
    {
        Assert.Multiple(() =>
        {
            Assert.That(_list.MaxOrDefault(0), Is.EqualTo(10));
            Assert.That(new List<int>().MaxOrDefault(0), Is.EqualTo(0));
        });
    }

    [Test]
    public void TestMinOrDefault()
    {
        Assert.Multiple(() =>
        {
            Assert.That(_list.MinOrDefault(0), Is.EqualTo(1));
            Assert.That(new List<int>().MinOrDefault(0), Is.EqualTo(0));
        });
    }

    [Test]
    public void TestShuffle()
    {
        List<int> originalList = new(_list);
        _list.Shuffle();
        Assert.That(_list, Is.Not.EqualTo(originalList));
    }

    [Test]
    public void TestShuffleTwice()
    {
        List<int> originalList = new(_list);
        _list.Shuffle();
        _list.Shuffle();
        Assert.That(_list, !Is.EqualTo(originalList));
    }

    [Test]
    public void TestShuffleEmptyList()
    {
        List<int> emptyList = new();
        emptyList.Shuffle();
        Assert.That(emptyList, Is.Empty);
    }

    [Test]
    public void TestShuffleSingleElementList()
    {
        List<int> singleElementList = new() {1};
        singleElementList.Shuffle();
        Assert.That(singleElementList, Has.Count.EqualTo(1));
    }

    [Test]
    public void TestShuffleTwoElementList()
    {
        List<int> twoElementList = new() {1, 2};
        twoElementList.Shuffle();
        Assert.That(twoElementList, Has.Count.EqualTo(2));
    }

    [Test]
    public void TestRemoveLastElement()
    {
        _list.RemoveLast();
        Assert.That(_list, Has.Count.EqualTo(9));
        Assert.That(_list, Does.Not.Contain(10));
    }

    [Test]
    public void TestRemoveLastElementEmptyList()
    {
        List<int> emptyList = new();
        Assert.That(emptyList.RemoveLast(), Is.False);
    }

    [Test]
    public void TestAddIfNotExists()
    {
        _list.AddIfNotExists(10);
        Assert.That(_list, Has.Count.EqualTo(10));
        _list.AddIfNotExists(11);
        Assert.That(_list, Has.Count.EqualTo(11));
        Assert.That(_list, Does.Contain(11));
    }
}