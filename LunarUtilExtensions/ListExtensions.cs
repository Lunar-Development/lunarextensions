namespace LunarUtilExtensions;

public static class ListExtensions
{

    /// <summary>
    ///  Splits a list into multiple lists of a specified size.
    /// </summary>
    /// <param name="size">Size of each list, default 10</param>
    /// <typeparam name="T">The type of the elements</typeparam>
    /// <returns>List of the split lists</returns>
    public static List<List<T>> Split<T>(this List<T> list, int size = 10)
    {
        var result = new List<List<T>>();
        for (int i = 0; i < list.Count; i += size)
        {
            result.Add(list.GetRange(i, Math.Min(size, list.Count - i)));
        }
        return result;
    }

    /// <summary>
    /// Checks if a list is null or has no elements.
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    /// <returns>True if the list is null or empty, false otherwise</returns>
    public static bool IsEmptyOrNull<T>(this List<T>? list)
    {
        return list == null || !list.Any();
    }

    /// <summary>
    /// Returns the maximum value in the list, or a default value if the list is empty.
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    /// <returns>The maximum value or the default value</returns>
    public static T? MaxOrDefault<T>(this List<T> list, T? defaultValue) where T : IComparable<T>
    {
        return list.Count != 0 ? list.Max() : defaultValue;
    }

    /// <summary>
    /// Returns the minimum value in the list, or a default value if the list is empty.
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    /// <returns>The minimum value or the default value</returns>
    public static T? MinOrDefault<T>(this List<T> list, T? defaultValue) where T : IComparable<T>
    {
        return list.Count != 0 ? list.Min() : defaultValue;
    }

    /// <summary>
    /// Shuffles the elements in the list.
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    public static void Shuffle<T>(this List<T> list)
    {
        Random rng = new();

        // Just to make sure that the shuffle is not in the same order
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            (list[k], list[n]) = (list[n], list[k]);
        }

    }

    /// <summary>
    /// Removes the last element from the list.
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    /// <returns>True if an element was removed, false otherwise</returns>
    public static bool RemoveLast<T>(this List<T> list)
    {
        if (list.Count == 0) return false;
        list.RemoveAt(list.Count - 1);
        return true;
    }

    /// <summary>
    /// Adds an element to the list only if it does not already exist in the list.
    /// </summary>
    /// <typeparam name="T">The type of the elements</typeparam>
    public static void AddIfNotExists<T>(this List<T> list, T item)
    {
        if (!list.Contains(item))
        {
            list.Add(item);
        }
    }

}
